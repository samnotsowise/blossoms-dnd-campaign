
# Help


Recently, I’ve taken up some new interests. These interests require a certain amount of stamina and endurance. The application of  the body to this task is great and most certainly willing. The staying power of said body is unfortunately, lacking.

To help rid me of this dysfunction I am working on a rather special concocktion.
It has been many long years since I last took up my alchemy and as such my stores have quite the dearth of useful ingredients. The component I am having most trouble locating is the Durraceel Bloom.
To any persons able to furnish me with a bouquet of these specialised flowers, I shall provide 1000 GP. 
Discretion is of the utmost importance – word of my needs getting out will result in the forfeiture of all compensation. It should also be noted that this is a rush order. No payment will be given for delivery after the middle of next week.

Many Thanks,
Anders Miinath
