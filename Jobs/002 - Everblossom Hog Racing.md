# Competitors At Your Marks

Good citizens of Everblossom. I, your mayor, am pleased to announce the turning of the decade will be marked by our famous hog racing festival.

The rules, as ever, are simple

 1.  No magically enhanced pork
 2. First porker to cross the finish line wins.
 3. No deviation from the marked course.
 4. Pigs may not fly.

This is an endurance race, so be sure to have your oinkment to hand.

The festivities will commence two weeks hence, so get your hogs trotting.

I look forward to a full rack of competitors competing for the mystery grand prize

Your Mayor

Nathaniel Wraith
