# Everblossom

## Events

### Festival
To mark the turning of a decade, Everblossom hosts a fabulour festival. People come from far and wide to participate in the parades, sample the exotic goods in the market, and watch the hog race (the climactic event, in the afternoon).

## Locations

### The Bear Trap
Gay bar, down by the docks - filled with greased up bears.

### The Hog's Head
A really dodgey looking bar. If you didn't know it was there, you'd assume it was just an old ruined house down a dark back alley.
