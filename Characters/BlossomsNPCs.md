# Blossoms NPCs

## Brünhilde Storm-Bringer
Carries a massive warhammer, which dances with electricity and a colossal black stone scutum shield. 
When not carrying the hammer she appears as a squat old woman with fire in her eyes,
who loves nothing more than to serve tea to the guild members who she dotes on like a grandmother. 
If a guild member steps out of line in her presence, they get flattened.
Hammer and attached shield can be summoned to her hand simply by her reaching in their direction.
As soon as the hammer touches her hand she takes on her true form -
a giantess entering her twilight years, fair and graceful with electric blue eyes and hair like wild fire.
She is not to be fucked with.

Quests:
*  [Icecream Friday](../Adventures/001%20-%20IceCreamFriday.md)

## Tabi
Big Tabaxi with jet black hair, and a mean scowl.
In combat is surrounded by 4 levitating weapons which he can command as well as the great sword he wields.
One of the most feared and respected members of Blossoms 
(unless he gets his hands on some milk. In that case, all bets are off).
