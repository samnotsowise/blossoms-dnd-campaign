# Everblossom NPCs

## Nathaniel Wraith (Mayor of Everblossom)
A flamboyant character who delights in terrible puns. 
Wears a regal purple robe and matching silk trousers. 
Has a large purple hat, topped with the large, exotic feathers.
Prefers to pit townsfolk against each other instead of doing any real administration himself.

**Quests:**
*  [Everblossom Hog Racing](../Jobs/002%20-%20Everblossom%20Hog%20Racing.md)