# Player Characters

Created by the party as characters to play. These are our heroes upon whom world events shall pivot.

**Current New Character Level:** 3

## HupHup
|Player|Race|Class|Level|
|--|--|--|--|
|Neal|Halfling (Male)|Fighter|4|

### Description
A weird looking little halfling. Sits astride a boar. Loves a full pint. Not a half one. He has usually had a full half one already. Spiky ginger hair. Boar is called Oink and carries his spear. Has a tiny voice. Just says "huphup". HupHup’s lance is called Hup-Hup-Hup which means shish-kebab.

### Notable Actions
* None

## Eugene Fitzerbert
|Player|Race|Class|Level|
|--|--|--|--|
|Ally|Human (Male)|Rogue|3|

### Description
Scoundrel. not as good looking as he thinks. has a nose that's just right. Loves all animal except white horses. Fond of Oink.

### Notable Actions
* None

## Oriaana
|Player|Race|Class|Level|
|--|--|--|--|
|Melissa|Tiefling (Female)|Wizard|3|

### Description
Looks like a sexy Tiefling. Black eyes, pale skin, red hair. Doesn't like people looking at her ~~tits~~ tail.

### Notable Actions
* None

## Tük
|Player|Race|Class|Level|
|--|--|--|--|
|Molly|Kenku (Male)|Bard|4|

### Description
Head and feathers of a fancy pigeon. Can speak common. Loves spicy gossip

### Notable Actions
* None

## Willy
|Player|Race|Class|Level|
|--|--|--|--|
|Harry|Human (Male)|Warlock|4|

### Description
Pretty famous back home. Lust for fame and glory. Loves to light a wee fire.

### Notable Actions
* None
