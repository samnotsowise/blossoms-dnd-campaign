The gang start the day by reading two job flyers, one for a [hog race in 2 weeks](../Jobs/002 - Everblossom Hog Racing.md), and another, [a plea for help](../Jobs/003 - Bounty: Wee Jimmy.md)

Gang prep for the Hog Race - head to the bar up the hill (The Hogs Head) and see if we can find The Pig Club to see what the odds are, and if we can buy pigs.

Bar was full of scum, no elves allowed.

One room pub, looks like someone's larder, large table and behind is a haggard old hag. She’s dressed in black with a pointed hat, she’s quite triangular. She is called Ma Fettridge.

We order drinks - Orianna uses her hip flask.

Everyone drinks a lot.

Anders Miinath has won it 4 years in a row. But we believe he is doping his pig.

Bets are made on the day, bring you pig and enter.

Ma tells us the bar will be very empty on the day - is she talking about underground betting or sex. Eugene is certain it's betting.

We decide to go see the bandits, Eugene and Billy are rote.

Up through arable land towards Whinsville, as we approach the edge of the forest we see a field full of dead cows, they’ve been butchered.

Omega brands on them. Eugene does an investigation to see if there are any signs of it being the bandits. It is clear that a group of people were involved. They have taken the sirloins…

Follows the trails of blood into the forest. Eugene scopes ahead, investigates for traps. We track them for about a quarter of a mile.

HupHup’s trusty steed Oink sniffs out the path after we lost it, it’s going along 200 metres parallel to the road.

We spot a farmer’s dwelling, thatched roof, farm animals, fire burning, and a path directly to the road.Lots of chickens are missing from the chicken house. Tuk knocks on the door, Orianna sizes her up and don’t like her nor she me. Eugene asks her about the bandits. They were burglared last week and the husband sent them off with a torch and a pitch fork (and their massive dog).

We move on and then discover a few forest dwellers home on fire. Random house fire starting. We stride in like heroes, Eugene falls over. We let them know we are going to stop them. Accelerant present on one of the houses, the 2nd is top down burning and is magical related.

Quarter of a mile from there we come across an odd clearing, the grass has all been trampled down in a worn path manner. We get to the far side of the clearing Orianna, Harry, Neil, we get hoiked into the air in a rope trap.Ally takes the rope and let’s us down, Orianna use Feather Fall so we aren’t hurt. It’s beautiful, except Oink runs off and HupHup can’t get him under control - skills challenge.
We succeed.

We are on the road and spot a peasant being chased by 4 bandits. Enter combat.

Thunderwave from Tuk.

HupHup skewers 2 bandits in a mighty surprise charge from the flank.

Eugene knocks remaining bandit unconscious with a frying pan.

We give peasant some water and she tells us the bandits are attacking.

William slaps the bandit awake, first with a glove, when that doesn’t work he puts it back on and slaps him with his hand. We interrogate the bandit and he tells us that Clappers is Wee Jimmy’s lieutenant and that there’s about 10 bandits up ahead.
  
Oriana takes the peasant back to town and brings the bandit to jail.  

We head towards the farm to kill bandits.  

HupHup kebab’s 1 bandit and half kebab’s another.  

Tuk shatter’s 3 bandits and kills all of them with an eagle’s screech.  

Billy red hand of ulster’s 2 bandits.

Clappers, flanks the party, then sends them flying with a surprise thunderwave.

Party battles Clappers, his head eventually gets exploded as he flees.

1 remaining bandit is detained.
