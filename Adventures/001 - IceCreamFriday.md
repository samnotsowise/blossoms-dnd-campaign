The party find themselves at the guild hall at the end of the day.

Brünhilde asks the party to go pick up and sort out the ice cream delivery from the docks.

Eugene mutters something vulgar. Brünhilde flattens him with her hammer as a warning.

The party heads off down the town to the docks.

Upon arriving at the dock the party find a colossal chunk of ice, and 6 pales of milk.

The party lash the milk to the ice and start trying to push it up the hill to the guild.

As they round the corner of the street, the ice slams over a bump in the cobble and a pale of milk falls loose.

Huphup dives to the assistance of the milk and manages to save 3/4 of it's contents.

The party to decide to look for a way to smooth the ice's journey. Billy and Eugene enter the Bear Trap bar and solicit
some members of staff for some of their spare grease.

With several pints of grease butter to hand, the party slicken up the cobbles and the ice glides onwards up the hill.

About halfway up the hill the party witness a woman having her baby snatched by someone who looks identical. 

Immediately upon hearing the cry for help, the party (minus billy, who doesn't give a fuck about commoners or babies) chase the culprit down a dark alleyway -
Huphup dispatches a poisonous rat which attempted to hinder the parties progress.

The team purse the villain down into the deep dark cellar of a house off the alley way.

The basement is unexpectedly tall and has large pipes around it's top.
The baby snatcher cries out in rage as it cannot jump high enough to escape into these pipes

The basement also features 4 stone slab tables, covered in what looks like humanoid remains.

The party engage the baby snatcher, who appears to be some sort of mimic.

The party, finding themselves under-prepared to battle such a strong foe,
decide to attempt to retrieve the baby and flee.

Scurrying can be heard from the pipes surrounding the room.

Huphup climbs up the mimic and wrestles the babe from it's magical grasp. 
He tosses the precious cargo to Eugene who bolts for the door.

An angry voice can be heard from up the stairs.

Eugene makes it to the stairs and bolts on up and out into the alleyway. 

The rest of the party attempt to flee from the mimic, but are cut off by a flood of poisonous rats who come pouring into
the basement.

A large, shadowy figure is glimpsed by Eugene as it barrels down the stairs.

Party in the basement attempt to fend off swarm of rats.

Tabi bursts into the basement, cries out "Guild Mates!" and launches headlong into battle.

The mimic is badly wounded by Tabi's opening salvo, and he cuts a swathe through the rats.

Tuk delivers a coup de grace to the mimic, and then books it out of the basement with the rest of the party.

The party return to where they left the ice and cream, Tabi in tow since they told him there was icecream.

The ice is gone, but the party see Billy walking down the street in his underwear.

Billy explains he engaged in some gambling, and used his winnings to pay some burley lads to transport the cargo up 
to the guild kitchen.

The party makes haste to the guild kitchen where they pull together as a team to create the frozen treat,
and keep Tabi and his milk thievery at bay.

Ice cream is served! Huphup rings the massive icecream/war bell at the top of the guild hall, and nearby members flock 
to the guild.

Eugene attempts to convince Huphup that the icecream is all gone once he's rung the bell. Huphup doesn't buy it. 
Eugene gets pancaked into a wall by Brünhilde's hurled warhammer. 
