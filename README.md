# Blossoms

## Premise
In an attempt to construct a more flexible DND environment, allowing for drop-in players, and regular players with variable availability, we have decided to model this game after an adventuring guild.

In the guild there are many characters. Some characters are player characters, other characters will never be playable, but will interact with the party, and may join the party. These characters are created by the DM to facilitate story, or by the party (with the DM's approval). Some of these characters (especially those suggested by the party), will be voiced by the party.

Members of the adventuring guild band together into parties, in order to take on jobs/quests.

## Player Characters

[List of All Player Characters](Characters/PlayerCharacters.md)

All members of the guild have agreed to unite behind the principles of the guild and the adventuring guild structure. This provides a unifying purpose for the party, giving boundaries for player character development. This allows players to flesh out their characters within binding constraints - they don't have to constantly justify to themselves why their character is there.

If players want to retire/shelve a character, in order to play as a new character, the guild structure provides for this. The character can simply leave the party and adventure with other, npc guild members (perhaps towards some great goal or ambition), or leave the guild entirely. Such characters may be sent on years long quests, in order to try and attain certain personal goals.

## Non-Player Characters
The guild contains many npcs. These fulfil a number of roles:
* They provide a mechanism to drive story.
* They allow for drop in characters for people who want to give DnD a try, and a safe place to stash those characters when they are done with, in case they need to be revisited.
* Party guardians/mentors - when the party wants to take on a difficult quest, it can so long as a sufficiently levelled npc agrees to accompany the party. The npc can guide the party through a particularly challenging quest. The reward in such a situation *may* be great. The risks **will** reflect the challenge rating, not the reward.

  * [Blossoms Guild NPCs](Characters/BlossomsNPCs.md)
  * [EverBlossom NPCs](Characters/EverBlossomsNPCs.md)
  * [World NPCs](Characters/WorldNPCs.md)
